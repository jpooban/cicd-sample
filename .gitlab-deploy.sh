server=$DEPLOY_SERVER

echo "Deploy project on server ${server}"    
ssh $server -i ~/.ssh/id_rsa "cd /var/www/html/ && rm -rf public && wget https://gitlab.com/JPooban/cicd-sample/-/jobs/artifacts/master/download?job=build && unzip download?job=build && rm -rf download?job=build"